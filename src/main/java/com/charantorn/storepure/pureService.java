/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charantorn.storepure;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class pureService {
    private static ArrayList<pureInput> pdtList = new ArrayList<>();
    private static pureInput currentUser = null;

    //Create 
    public static boolean addData(pureInput data){
        pdtList.add(data);

        return true;        
    }
    //Delete ////
    public static boolean delData(pureInput data){
        pdtList.remove(data);

        return true;        
    }
    //DeleteOverload
    public static boolean delData(int index){
        pdtList.remove(index);

        return true;        
    }
    //Read ////////
    public static ArrayList<pureInput> getdata(){
        return pdtList;
    }
    public static pureInput getUser(int index){
        return pdtList.get(index);
    }
}
