/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charantorn.storepure;



/**
 *
 * @author User
 */
public class pureInput {
    private String pdtName;
    private String pdtBrand;

    public pureInput(String pdtName, String pdtBrand ) {
        this.pdtName = pdtName;
        this.pdtBrand = pdtBrand;
        
    }

    public String getPdtName() {
        return pdtName;
    }

    public void setPdtName(String pdtName) {
        this.pdtName = pdtName;
    }

    public String getPdtBrand() {
        return pdtBrand;
    }

    public void setPdtBrand(String pdtBrand) {
        this.pdtBrand = pdtBrand;
    }

    @Override
    public String toString() {
        return " pdtName=" + pdtName + ", pdtBrand =" + pdtBrand + '}';
    }

    
}
